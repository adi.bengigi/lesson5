#include "Circle.h"


void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), this->getRadius(), BLACK).display(disp);
}
Circle::~Circle()
{
	// free allocated memory
	for (int i = 0; i < this->_graphicShape.size(); i++)
		delete this->_graphicShape[i];
}
Circle::Circle(const Point& center, double radius, const string& type, const string& name) :
	_raduis(radius), Shape(name, type)
{
	this->_center += center;
}
const Point& Circle::getCenter() const
{
	return this->_center;
}
double Circle::getRadius() const
{
	return this->_raduis;
}
double Circle::getArea() const
{
	return 3.14 * pow(this->_raduis, 2);
}
double Circle::getPerimeter() const
{
	return 3.14 * 2 * this->_raduis;
}
void Circle::move(const Point& other)
{
	this->_center += other;
}
