#include <string>
#include "Shape.h"

Shape::Shape(const string& name, const string& type) :
	_name(name), _type(type)
{}
/**
* Prints to Cout the details about the shape
**/
void Shape::printDetails()const
{
	std::cout << "Type: " << this->_type << ", Name: " << this->_name << ", Area: " << this->getArea() << ", Perimeter: " << this->getPerimeter() << std::endl;
}
string Shape::getType() const
{
	return this->_type;
}
string Shape::getName() const
{
	return this->_name;
}
