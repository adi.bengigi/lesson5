#include "Rectangle.h"


void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name)
	: Polygon(type, name)
{
	if (length == 0 || width == 0) {
		throw std::exception("There is an error in your ractangle, it's area is 0!");
	}
	this->length = length;
	this->width = width;
	Point p2(a);
	Point temp(length, width);
	p2 += temp;
	this->_points.push_back(a);
	this->_points.push_back(p2);
}
myShapes::Rectangle::~Rectangle() {

}

double myShapes::Rectangle::getArea() const {
	return this->length * this->width;
}
double myShapes::Rectangle::getPerimeter() const {
	return (this->length + this->width) * 2;
}
void myShapes::Rectangle::move(const Point& other)
{
	Point t(this->length, this->width);
	t += other;
	this->_points[0] = other;
	this->_points[1] = t;
}
