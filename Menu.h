#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


enum TYPES {
	CIRCLE, ARROW, TRIANGLE, RECTANGLE
};
enum MODIFIY_ACTIONS {
	MOVE, PRINT_D, REMOVE
};
enum TASKS {
	ADD, MODIFY, REMOVEALL, EXIT
};

class Menu
{
public:

	Menu();
	~Menu();

	// more functions..
	void menu();
	void CreateShapeMenu();
	void ModifyOption();
	void RemoveAllOption();
private:
	void printList(); // prints a list of all shapes
	void ModifyShapeMenu(int id); // given shape, modify it
	void MoveShapeMenu(int id); // given shape, get values from user and move it
	void createShape(const int type); // this functions creates new shape and adds it to the shapes list
	void moveShape(Point& newPoint, int id); // moves the shape in id to newPoint
	void removeShape(int id); // removes the shape in id
	void removeAll(); // deletes all shapes from canvas
	void drawAll(); // draws all shapes
	Point * EnterNPoints(int n); // enter N points to array, MUST be freed
	vector<Shape*> shapes;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

