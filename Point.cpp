#include <cmath>
#include "Point.h"


Point::Point(double x, double y) : _x(x), _y(y) {}
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}
Point::~Point() {}
Point& Point::operator+(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}
Point& Point::operator+=(const Point &other)
{
	*this = *this + other;
	return *this;
}
double Point::getX() const
{
	return this->_x;
}
double Point::getY() const
{
	return this->_y;
}
double Point::distance(const Point& other) const
{
	double distance = sqrt(pow((this->_x - other.getY()), 2) + pow((this->_y - other.getY()), 2));
	return distance;
}