#include "Menu.h"


Menu::Menu()
{
	_board = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}
// enter N points to array and return it
Point * Menu::EnterNPoints(int n)
{
	Point * pointsArray = new Point[n];
	double xVal, yVal;
	for (int i = 0; i < n; i++)
	{
		std::cout << "Enter the X of point number: " << i + 1 << std::endl;
		cin >> xVal;
		std::cout << "Enter the Y of point number: " << i + 1 << std::endl;
		cin >> yVal;
		pointsArray[i] = Point(xVal, yVal);
	}
	return pointsArray;
}
// display the menu and add a new shape
void Menu::createShape(const int type)
{
	switch (type)
	{
	case ARROW:
	{
		Point * points = this->EnterNPoints(2);
		string name;
		std::cout << "Enter name for shape: " << std::endl;
		cin >> name;
		this->removeAll();
		Arrow* a = new Arrow(points[0], points[1], "Arrow", name);
		this->shapes.push_back(a);
		delete[] points;
		break;
	}
	case CIRCLE:
	{
		Point * points = this->EnterNPoints(1);
		string name;
		double radius;
		std::cout << "Enter radius: " << std::endl;
		cin >> radius;
		std::cout << "Enter name for shape: " << std::endl;
		cin >> name;
		this->removeAll();
		Circle *c = new Circle(points[0], radius, "Circle", name);
		this->shapes.push_back(c);
		delete[] points;
		break;
	}
	case TRIANGLE:
	{
		Point * points = this->EnterNPoints(3);
		string name;
		std::cout << "Enter name for shape: " << std::endl;
		cin >> name;
		try {
			this->removeAll();
			Triangle *t = new Triangle(points[0], points[1], points[2], "Triangle", name);
			this->shapes.push_back(t);
		}
		catch (std::exception e) {
			std::cout << e.what() << std::endl;
			system("pause");
		}
		delete[] points;
		break;
	}
	case RECTANGLE: // this one have different messages, so i won't use the EnterNPoints for this
	{
		Point p;
		double leftCornerX, leftCornerY;
		double length = 0, width = 0;
		string name;
		std::cout << "Enter the X of the to left corner:" << std::endl;
		std::cin >> leftCornerX;
		std::cout << "Enter the Y of the to left corner:" << std::endl;
		std::cin >> leftCornerY;
		std::cout << "Enter length of the shape: " << std::endl;
		std::cin >> length;
		std::cout << "Enter width of the shape: " << std::endl;
		std::cin >> width;
		std::cout << "Enter name for shape: " << std::endl;
		std::cin >> name;
		p = Point(leftCornerX, leftCornerY);
		try {
			this->removeAll();
			myShapes::Rectangle *r = new myShapes::Rectangle(p, length, width, "Rectangle", name);
			this->shapes.push_back(r);
		}
		catch (std::exception e)
		{
			std::cout << e.what() << std::endl;
			system("pause");
		}
		break;
	}
	}
	this->drawAll();
}
// draw all shapes
void Menu::drawAll()
{
	for (int i = 0; i < this->shapes.size(); i++)
	{
		this->shapes[i]->draw(*this->_disp, *this->_board);
	}
}
// delete all shapes
void Menu::removeAll()
{
	for (int i = 0; i < this->shapes.size(); i++)
	{
		this->shapes[i]->clearDraw(*this->_disp, *this->_board);
	}
}
// remove the id'nt shape
void Menu::removeShape(int id)
{
	this->removeAll();
	this->shapes.erase(this->shapes.begin() + id); // removed shape from vector
	this->drawAll(); // now it's gone
}
// move the id'nt shape to point
void Menu::moveShape(Point& newPoint, int id)
{
	this->removeAll();
	this->shapes[id]->move(newPoint);
	this->drawAll();
}
// display the move shape menu and move it
void Menu::MoveShapeMenu(int id)
{
	Point newP;
	double newX, newY;
	std::cout << "Please enter the X moving scale: " << std::endl;
	cin >> newX;
	std::cout << "Please enter the Y moving scale: " << std::endl;
	cin >> newY;
	newP = Point(newX, newY);
	this->removeAll();
	this->shapes[id]->move(newP);
	this->drawAll();
}
// display the modify menu and do somthing
void Menu::ModifyShapeMenu(int id)
{
	int action;
	std::cout << "Enter 0 to move the Shape.\nEnter 1 to get its details.\nEnter 2 to remove the shape." << std::endl;
	cin >> action;
	switch (action)
	{
	case MOVE:
		this->MoveShapeMenu(id);
		break;
	case PRINT_D:
		this->shapes[id]->printDetails();
		system("pause");
		break;
	case REMOVE:
		this->removeShape(id);
		break;
	default:
		std::cout << "I don't know what you want, so i will print the shape..." << std::endl;
		this->shapes[id]->printDetails();
		system("pause");
		break;
	}
}
// print all shapes and select one
void Menu::ModifyOption()
{
	int id = -1;
	if (this->shapes.size() != 0) {
		do {
			std::cout << "" << std::flush; // cleared the console window
			this->printList();
			cin >> id;
		} while (id < 0 || id > this->shapes.size());
		this->ModifyShapeMenu(id);
	}
	else
		std::cout << std::flush;
}
// i'm not going to explain these two, you can undersand it
void Menu::RemoveAllOption()
{
	this->removeAll();
	this->shapes.clear();
}
void Menu::printList()
{
	for (int i = 0; i < this->shapes.size(); i++)
	{
		std::cout << "Enter" << i << "for " << this->shapes[i]->getName() << "(" << this->shapes[i]->getType() << ")" << std::endl;
	}
}
// display the create shape menu
void Menu::CreateShapeMenu()
{
	int type;
	do {
		std::cout << std::flush;
		std::cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle." << std::endl;
		std::cin >> type;
	} while (type < CIRCLE || type > RECTANGLE);
	this->createShape(type);
}
// display the main menu
void Menu::menu() {
	int action;
	do
	{
		std::cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit." << std::endl;
		std::cin >> action;
	} while (action < ADD || action > EXIT);
	switch (action)
	{
	case ADD:
		this->CreateShapeMenu();
		break;
	case MODIFY:
		this->ModifyOption();
		break;
	case REMOVEALL:
		this->RemoveAllOption();
		break;
	case EXIT:
		exit(0);
	}

}