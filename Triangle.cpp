#include "Triangle.h"

bool onSameExis(const Point& p1, const Point& p2, const Point& p3)
{
	return (p1.getX() == p2.getX() && p1.getX() == p3.getX() && p2.getX() == p3.getX()) || (p1.getY() == p2.getY() && p1.getY() == p3.getY() && p2.getY() == p3.getY());
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name)
	: Polygon(type, name), Shape(name, type)
{
	if (onSameExis(a, b, c))
	{
		std::cerr << "There is an error in your ractangle, it's area is 0!" << std::endl;
		throw std::exception("Points are not valid");
	}
	this->_points = { a, b, c };
}
// we malloced nothing, so we don't need to free
Triangle::~Triangle() {}


double Triangle::getArea() const
{
	// by using the formula S = |0.5[x1(y2 - y3) + x2(y3 - y1) + x3(y1 - y2)]|
	return abs(0.5 * (this->_points[0].getX()*(this->_points[1].getY() - this->_points[2].getY()) + \
		this->_points[1].getX()*(this->_points[2].getY() - this->_points[0].getY() + \
			this->_points[2].getX()*(this->_points[0].getY() - this->_points[1].getY()))));
}
double Triangle::getPerimeter()const
{
	return this->_points[0].distance(this->_points[1])\
		+ this->_points[1].distance(this->_points[2])\
		+ this->_points[2].distance(this->_points[0]);
}
void Triangle::move(const Point& other)
{
	this->_points[0] += other;
	this->_points[1] += other;
	this->_points[2] += other;
}